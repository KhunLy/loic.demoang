import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-translation',
  templateUrl: './translation.component.html',
  styleUrls: ['./translation.component.scss']
})
export class TranslationComponent implements OnInit {

  constructor(private transtaltion: TranslateService) { }

  ngOnInit(): void {

    // doit etre dans le app.module.ts
    this.transtaltion.addLangs(["fr", "en"]);

    
    this.transtaltion.use("en");
  }

}
