import { Component, OnInit } from '@angular/core';
import { ChatServiceService } from '../chat-service.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {

  model: string[];
  newItem: string;


  constructor(private chatService: ChatServiceService) {
    chatService.Message$.subscribe(data => this.model = data);
  }

  click() {
    this.chatService.post(this.newItem);
    this.newItem = null;
  }

  ngOnInit(): void {
  }

}
