import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatServiceService{

  private hub : signalR.HubConnection

  private _messages$: BehaviorSubject<string[]>;


  public get Message$(): Observable<string[]> {
    return this._messages$.asObservable();
  }

  constructor() { 

    this._messages$ = new BehaviorSubject<string[]>(null);
    this.hub= new signalR.HubConnectionBuilder()
                            .withUrl('http://localhost:57017/chathub')
                            .build();
    this.hub.start().then(() => {
      this.hub.invoke("GetAll");
    });
    
    this.hub.on(
      "Refresh", 
      (data) => { 
        this._messages$.next(data) 
      })
  }

  public post(item) {
    this.hub.invoke("Post", item);
  }


}
